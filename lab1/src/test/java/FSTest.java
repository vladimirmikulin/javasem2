import FS.Directory;
import FS.MyOwn;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("File system")
public class FSTest {
    @Test
    @DisplayName("Move files")
    void moveFilesTest() {
        var root = Directory.create("", null);
        var myOwn = MyOwn.create("pipe", root);
        var homeDir = Directory.create("home", root);
        var userDir = Directory.create("user", homeDir);

        myOwn.move("/home/user/");
        assertFalse(userDir.list().isEmpty());
        assertEquals(myOwn, userDir.list().get(0));

        userDir.move("/");
        assertTrue(root.list().contains(userDir));
    }

    @Test
    @DisplayName("Delete files")
    void deleteFilesTest() {
        var root = Directory.create("", null);
        var homeDir = Directory.create("home", root);
        var myOwn = MyOwn.create("pipe", homeDir);
        var tempDir = Directory.create("user", homeDir);

        myOwn.delete();
        assertFalse(homeDir.list().contains(myOwn));
        assertTrue(homeDir.list().contains(tempDir));
        tempDir.delete();
        assertFalse(homeDir.list().contains(tempDir));
        homeDir.delete();
        assertTrue(root.list().isEmpty());
    }
}
