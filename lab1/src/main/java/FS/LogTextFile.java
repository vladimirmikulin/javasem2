package FS;

import java.util.concurrent.Semaphore;

public class LogTextFile extends FMain {
    private StringBuilder data;
    private final Semaphore semaphore;
    private int countReaders;

    private LogTextFile(String name, Directory parent, String data) {
        super(name, parent);
        this.data = new StringBuilder(data);
        semaphore = new Semaphore(1);
    }

    public static LogTextFile create(String name, Directory parent, String data) {
        if (data == null) {
            throw new IllegalArgumentException("Data should not be null");
        }
        return new LogTextFile(name, parent, data);
    }

    public String read() {
        synchronized (semaphore) {
            if (countReaders++ == 0) {
                semaphore.acquireUninterruptibly();
            }
        }
        try {
            return data.toString();
        } finally {
            synchronized (semaphore) {
                if (--countReaders == 0) {
                    semaphore.release();
                }
            }
        }
    }

    public void append(String line) {
        if (line == null) {
            throw new IllegalArgumentException("Line should not be null");
        }
        semaphore.acquireUninterruptibly();
        try {
            data.append(line);
        } finally {
            semaphore.release();
        }
    }

    @Override
    public FMainType getType() {
        return FMainType.LOG_TEXT_FILE;
    }
}
