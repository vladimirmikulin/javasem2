package FS;

import java.util.concurrent.Semaphore;

public class MyOwn extends FMain {
    private String data;
    private Semaphore semaphoreReader;
    private Semaphore semaphoreWriter;

    private MyOwn(String name, Directory parent) {
        super(name, parent);
        data = "";
        semaphoreReader = new Semaphore(1);
        semaphoreWriter = new Semaphore(1);
        semaphoreReader.acquireUninterruptibly();
    }

    public static MyOwn create(String name, Directory parent) {
        return new MyOwn(name, parent);
    }

    public void write(String data) {
        if (data == null) {
            throw new IllegalArgumentException("Data should not be null");
        }
        semaphoreWriter.acquireUninterruptibly();
        this.data = data;
        semaphoreReader.release();
    }

    public String read() {
        semaphoreReader.acquireUninterruptibly();
        var res = data;
        semaphoreWriter.release();
        return res;
    }

    @Override
    public FMainType getType() {
        return FMainType.MY_OWN;
    }
}
