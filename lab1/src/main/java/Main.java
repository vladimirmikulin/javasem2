import FS.*;

import Threads.Task3;

public class Main {
    public static void main(String[] args) {
        System.out.println("______________________________ Lab #1 by Volodymyr Mikulin");
        System.out.println("Task 2");
        var root = Directory.create("root", null);
        var userDir = Directory.create("user", Directory.create("home", root));
        var sysDir = Directory.create("sys", root);
        var binaryFile = BinaryFile.create("data.bin", sysDir, "dat".getBytes());
        var bufferFile = BufferFile.<Character>create("buff.sys", sysDir);
        var logFile = LogTextFile.create("sample.log", root, "some log\n");
        var myOwn = MyOwn.create("own-node", sysDir);

        fsTraverse(root, 0);

        logFile.append("Hello\n");
        logFile.append("some data\n");
        logFile.move(userDir);

        System.out.println();
        fsTraverse(root, 0);

        System.out.println();
        userDir.list().forEach((x) ->  {
            if (x.getType() == FMain.FMainType.LOG_TEXT_FILE) {
                System.out.println(((LogTextFile)x).read());
            }
        });

        binaryFile.move("/");

        System.out.println();
        fsTraverse(root, 0);

        System.out.println(new String(binaryFile.read()));

        bufferFile.push('V');
        bufferFile.push('A');
        System.out.println(bufferFile.consume());

        userDir.move("/sys");
        System.out.println();
        fsTraverse(root, 0);

        myOwn.write("data own-node");
        System.out.println(myOwn.read());
        System.out.println("=====================================");
        System.out.println("Task 3");
        Task3 one = new Task3(1) {
            @Override
            protected void phaseTwo() {
                try {
                    countDownLatchCount2.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Task3 two = new Task3(2) {
            @Override
            protected void phaseTwo() {
                try {
                    countDownLatchCount2.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Task3 three = new Task3(3) {
            @Override
            protected void phaseTwo() { }

            @Override
            public void phaseThree() {
                countDownLatchCount2.countDown();
                super.phaseThree();
            }
        };
        Task3 four = new Task3(4) {
            @Override
            protected void phaseTwo() { }

            @Override
            public void phaseThree() {
                countDownLatchCount2.countDown();
                super.phaseThree();
            }
        };
        Task3 five = new Task3(5) {
            @Override
            protected void phaseTwo() {
                try {
                    countDownLatchCount4.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void phaseThree() { }
        };
        one.start();
        two.start();
        three.start();
        four.start();
        five.start();

        try {
            one.join();
            two.join();
            three.join();
            four.join();
            five.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void fsTraverse(Directory dir, int indent) {
        System.out.println(" ".repeat(indent) + entityPrint(dir));
        indent += 4;
        for (var cur : dir.list()) {
            if (cur.isDirectory()) {
                fsTraverse((Directory) cur, indent);
            } else {
                System.out.println(" ".repeat(indent) + entityPrint(cur));
            }
        }
    }

    private static String entityPrint(FMain e) {
        return "-=>>>> " + e.getType().toString() + ": " + e.getName();
    }
}
