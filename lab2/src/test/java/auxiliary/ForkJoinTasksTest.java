package auxiliary;

import static org.junit.jupiter.api.Assertions.*;
import FS.BinaryFile;
import FS.Directory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Fork join task test")
public class ForkJoinTasksTest {
    @Test
    @DisplayName("Search")
    void search() {
        var root = Directory.create("", null);
        var sysDir = Directory.create("sys", root);
        var homeDir = Directory.create("home", root);
        var userDir = Directory.create("user", homeDir);
        var dataBin1 = BinaryFile.create("data", homeDir, "data".getBytes());
        var dataBin2 = BinaryFile.create("data", root, "data".getBytes());
        var dataBin3 = BinaryFile.create("data", userDir, "data".getBytes());
        assertEquals(root.search("^.*data.*$").size(), 3);
        assertFalse(root.search("^.*data.*$").contains("root/data"));
        assertFalse(root.search("^.*data.*$").contains("root/home/data"));
        assertFalse(root.search("^.*data.*$").contains("root/home/user/data"));
    }

    @Test
    @DisplayName("Count")
    void count() {
        var root = Directory.create("", null);
        var sysDir = Directory.create("sys", root);
        var homeDir = Directory.create("home", root);
        var userDir = Directory.create("user", homeDir);
        var userBin = BinaryFile.create("bin", homeDir, "data".getBytes());
        assertEquals(4, root.count(true));
    }

    @Test
    @DisplayName("Tree")
    void tree() {
        var root = Directory.create("", null);
        var sysDir = Directory.create("sys", root);
        var homeDir = Directory.create("home", root);
        var userDir = Directory.create("user", homeDir);
        var dataBin1 = BinaryFile.create("data", homeDir, "data".getBytes());
        var dataBin2 = BinaryFile.create("data", root, "data".getBytes());
        var dataBin3 = BinaryFile.create("data", userDir, "data".getBytes());
        assertEquals(3, root.getEntitiesWithSizeInRange(3, 5).size());
    }
}