package FS.actions;

import FS.BufferFile;
import FS.Directory;
import FS.LogTextFile;
import FS.NamedPipe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import FS.exceptions.ArgumentNotSetException;
import FS.exceptions.ArgumentOverrideException;

import FS.exceptions.SimilarArgumentSetException;

@DisplayName("FS Actions")
public class    FSActionsTest {
    private Directory root;

    @BeforeEach
    private void createRootDir() {
        root = Directory.create("/", null);
    }

    @Test
    @DisplayName("Append action")
    void appendActionTest() {
        var logTextFile = LogTextFile.create("test.log", root, "data\n");
        var appendActionFromObj = new Append(logTextFile);
        var appendActionFromPath = new Append("/test.log");

        assertEquals(FSAction.FSActionType.APPEND, appendActionFromObj.getFSActionType());
        assertEquals(FSAction.FSActionType.APPEND, appendActionFromPath.getFSActionType());

        assertFalse(appendActionFromObj.getTarget().isEmpty());
        assertTrue(appendActionFromObj.getTargetPath().isEmpty());

        assertTrue(appendActionFromPath.getTarget().isEmpty());
        assertFalse(appendActionFromPath.getTargetPath().isEmpty());

        assertThrows(ArgumentNotSetException.class, appendActionFromObj::getArgument);
        assertThrows(ArgumentNotSetException.class, appendActionFromPath::getArgument);

        assertDoesNotThrow(() -> appendActionFromObj.setArgument("data_test"));
        assertDoesNotThrow(() -> appendActionFromPath.setArgument("DaTa tesT"));

        assertThrows(ArgumentOverrideException.class, () -> appendActionFromObj.setArgument("1"));
        assertThrows(ArgumentOverrideException.class, () -> appendActionFromObj.setArgument("1"));

        assertDoesNotThrow(appendActionFromObj::getArgument);
        assertDoesNotThrow(appendActionFromPath::getArgument);

        try {
            assertEquals("data_test", appendActionFromObj.getArgument());
            assertEquals("DaTa tesT", appendActionFromPath.getArgument());
        } catch (ArgumentNotSetException e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Consume action")
    void consumeActionTest() {
        var bufferFile = BufferFile.<Character>create("test", root);
        var consumeActionFromObj = new Consume<>(bufferFile);
        var consumeActionFromPath = new Consume<>("/test");

        assertEquals(FSAction.FSActionType.CONSUME, consumeActionFromObj.getFSActionType());
        assertEquals(FSAction.FSActionType.CONSUME, consumeActionFromPath.getFSActionType());
        assertFalse(consumeActionFromObj.getTarget().isEmpty());
        assertTrue(consumeActionFromObj.getTargetPath().isEmpty());
        assertTrue(consumeActionFromPath.getTarget().isEmpty());
        assertFalse(consumeActionFromPath.getTargetPath().isEmpty());
        assertEquals(bufferFile, consumeActionFromObj.getTarget().get());
        assertEquals("/test", consumeActionFromPath.getTargetPath().get());
        assertThrows(ArgumentNotSetException.class, consumeActionFromObj::getArgument);
        assertThrows(ArgumentNotSetException.class, consumeActionFromPath::getArgument);
    }

    @Test
    @DisplayName("Count action")
    void countActionTest() {
        var countActionFromObj = new Count(root);
        var countActionFromPath = new Count("/");

        assertEquals(FSAction.FSActionType.COUNT, countActionFromObj.getFSActionType());
        assertEquals(FSAction.FSActionType.COUNT, countActionFromPath.getFSActionType());

        assertEquals(root, countActionFromObj.getTarget().get());
        assertEquals("/", countActionFromPath.getTargetPath().get());
    }

    @Test
    @DisplayName("Create action")
    void createActionTest() {
        var createAction = new Create<>(Directory.class);

        assertEquals(FSAction.FSActionType.CREATE, createAction.getFSActionType());
        assertEquals(Directory.class, createAction.getClassOfTarget());
        assertThrows(ArgumentNotSetException.class, createAction::getNameArgument);
        assertDoesNotThrow(() -> createAction.setNameArgument("build"));
        assertThrows(ArgumentOverrideException.class, () -> createAction.setNameArgument("1"));
        try {
            assertEquals("build", createAction.getNameArgument());
        } catch (ArgumentNotSetException e) {
            fail(e.getMessage());
        }

        assertThrows(ArgumentNotSetException.class, createAction::getDirectoryArgument);
        assertThrows(ArgumentNotSetException.class, createAction::getDirectoryPathArgument);

        assertDoesNotThrow(() -> createAction.setDirectoryPathArgument("/lib"));
        assertThrows(ArgumentOverrideException.class, () -> createAction.setDirectoryPathArgument("/"));
        assertThrows(SimilarArgumentSetException.class, () -> createAction.setDirectoryArgument(root));

        try {
            assertEquals("/lib", createAction.getDirectoryPathArgument().get());
            assertTrue(createAction.getDirectoryArgument().isEmpty());
        } catch (ArgumentNotSetException e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Other create actions")
    void createActionOthersTest() {
        var createBinaryFileAction = new CreateBinaryFile();
        var createLogTextFileAction = new CreateLogTextFile();

        assertDoesNotThrow(() -> createBinaryFileAction.setBytesArgument(new byte[] {0, 1, 2}));
        assertDoesNotThrow(() -> createLogTextFileAction.setDataArgument("data"));
        try {
            assertEquals("data", createLogTextFileAction.getDataArgument());
            assertArrayEquals(new byte[] {0, 1, 2}, createBinaryFileAction.getBytesArgument());
        } catch (ArgumentNotSetException e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Actions without params")
    void actionsWithoutParamsTest() {
        assertEquals(FSAction.FSActionType.DELETE, new Delete("/dir").getFSActionType());
        assertEquals(FSAction.FSActionType.DELETE, new Delete(root).getFSActionType());

        assertEquals(FSAction.FSActionType.GET_NAME, new GetName("/test").getFSActionType());
        assertEquals(FSAction.FSActionType.GET_NAME, new GetName(root).getFSActionType());

        assertEquals(FSAction.FSActionType.GET_SIZE, new GetSize("/tmp").getFSActionType());
        assertEquals(FSAction.FSActionType.GET_SIZE, new GetSize(root).getFSActionType());

        assertEquals(FSAction.FSActionType.GET_TYPE, new GetType("/").getFSActionType());
        assertEquals(FSAction.FSActionType.GET_TYPE, new GetType(root).getFSActionType());

        assertEquals(FSAction.FSActionType.IS_DIRECTORY, new IsDirectory("/").getFSActionType());
        assertEquals(FSAction.FSActionType.IS_DIRECTORY, new IsDirectory(root).getFSActionType());

        assertEquals(FSAction.FSActionType.LIST, new List("/").getFSActionType());
        assertEquals(FSAction.FSActionType.LIST, new List(root).getFSActionType());

        var buffFile = BufferFile.<Character>create("test.buf", root);
        assertEquals(FSAction.FSActionType.PUSH, new Push<>("/test.buf").getFSActionType());
        assertEquals(FSAction.FSActionType.PUSH, new Push<Character>(buffFile).getFSActionType());

        var lofFile = LogTextFile.create("log", root, "data");
        assertEquals(FSAction.FSActionType.READ, new Read<>("/log").getFSActionType());
        assertEquals(FSAction.FSActionType.READ, new Read<>(lofFile).getFSActionType());
        assertEquals(FSAction.FSActionType.SEARCH, new Search("/").getFSActionType());
        assertEquals(FSAction.FSActionType.SEARCH, new Search(root).getFSActionType());
        assertEquals(FSAction.FSActionType.TREE, new Tree("/").getFSActionType());
        assertEquals(FSAction.FSActionType.TREE, new Tree(root).getFSActionType());

        var pipe = NamedPipe.create("pipe", root);
        assertEquals(FSAction.FSActionType.WRITE, new Write("/").getFSActionType());
        assertEquals(FSAction.FSActionType.WRITE, new Write(pipe).getFSActionType());
    }

    @Test
    @DisplayName("Get entities with size in range acton")
    void getEntitiesWithFilterTest() {
        var getEntitiesWithFilterActionFromObj = new GetEntitiesWithSizeInRange(root);
        var getEntitiesWithFilterActionFromPath = new GetEntitiesWithSizeInRange("/");

        assertEquals(
                FSAction.FSActionType.GET_ENTITIES_WITH_SIZE_IN_RANGE,
                getEntitiesWithFilterActionFromObj.getFSActionType());

        assertEquals(
                FSAction.FSActionType.GET_ENTITIES_WITH_SIZE_IN_RANGE,
                getEntitiesWithFilterActionFromPath.getFSActionType());

        assertDoesNotThrow(() -> getEntitiesWithFilterActionFromObj.setLowerBoundArgument(23));
        assertDoesNotThrow(() -> getEntitiesWithFilterActionFromObj.setUpperBoundArgument(1223));

        assertThrows(ArgumentNotSetException.class, getEntitiesWithFilterActionFromPath::getLowerBoundArgument);
        try {
            assertEquals(23, getEntitiesWithFilterActionFromObj.getLowerBoundArgument());
            assertEquals(1223, getEntitiesWithFilterActionFromObj.getUpperBoundArgument());
        } catch (ArgumentNotSetException e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Move acton")
    void moveTest() {
        var moveActionFromObj = new Move(root);
        var moveActionFromPath = new Move("/");

        assertEquals(FSAction.FSActionType.MOVE, moveActionFromObj.getFSActionType());
        assertEquals(FSAction.FSActionType.MOVE, moveActionFromPath.getFSActionType());
        assertDoesNotThrow(() -> moveActionFromObj.setDestinationArgument(root));
        assertThrows(SimilarArgumentSetException.class, () -> moveActionFromObj.setDestinationPathArgument("/tmp"));
        try {
            assertEquals(root, moveActionFromObj.getDestinationArgument().get());
        } catch (ArgumentNotSetException e) {
            fail(e.getMessage());
        }
    }
}
