import FS.BinaryFile;
import FS.BufferFile;
import FS.Directory;
import FS.LogTextFile;
import FS.actions.Count;
import FS.actions.Append;
import FS.exceptions.ArgumentOverrideException;
import FS.exceptions.SimilarArgumentSetException;
import auxiliary.Controller;
import auxiliary.Generator;
import java.util.concurrent.ExecutionException;

public class Main {
    public static void main(String[] args) throws ArgumentOverrideException, SimilarArgumentSetException {
        System.out.println("Lab2 by Volodymyr Mikulin");
        var root = Directory.create("root", null);
        var tmp = Directory.create("tmp", root);
        var var = Directory.create("var", root);
        var home = Directory.create("home", root);
        var user = Directory.create("user", home);
        var projects = Directory.create("projects", user);
        var logFile = LogTextFile.create("log_file.txt", projects, "Some data");
        var binaryFile = BinaryFile.create("bin_file", var, new byte[] {0, 2 ,3 ,4});
        var buffFile = BufferFile.create("buff_file", user);

        System.out.println("Tree json");
        System.out.println(root.tree());
        System.out.println("Count all: ");
        System.out.println(root.count(true));
        System.out.println("Count in root: ");
        System.out.println(root.count(false));
        System.out.println("Search with size > 3: ");
        root.getEntitiesWithSizeInRange(3, Integer.MAX_VALUE)
                .forEach(x -> System.out.println(x.getFullPath()));
        System.out.println("Search files with name *file*");
        root.search("^.*file.*$").forEach(System.out::println);

        Generator g = new Generator(new Controller());
        g.performFSStressTest();

    }
}
