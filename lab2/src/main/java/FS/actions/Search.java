package FS.actions;

import FS.Directory;


public class Search extends FSAction<String, Directory> {
    public Search(Directory target) {
        super(target);
    }

    public Search(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.SEARCH;
    }
}
