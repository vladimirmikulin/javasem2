package FS.actions;

import FS.Directory;


public class Count extends FSAction<Boolean, Directory> {
    public Count(Directory target) {
        super(target);
    }

    public Count(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.COUNT;
    }
}

