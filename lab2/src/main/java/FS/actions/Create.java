package FS.actions;

import FS.Directory;
import FS.Entity;
import FS.exceptions.ArgumentNotSetException;
import FS.exceptions.ArgumentOverrideException;
import FS.exceptions.SimilarArgumentSetException;

import java.util.Optional;

public class Create<T extends Entity> extends FSAction<String, T> {
    private Directory directory = null;
    private String directoryPath = null;

    public Create(Class<T> clazz) {
        super(clazz);
    }

    public Class<T> getClassOfTarget() {
        return entityType;
    }

    public void setNameArgument(String name) throws ArgumentOverrideException {
        setArgument(name);
    }

    public String getNameArgument() throws ArgumentNotSetException {
        return getArgument();
    }

    public void setDirectoryArgument(Directory dir) throws ArgumentOverrideException, SimilarArgumentSetException {
        if (directory != null) {
            throw new ArgumentOverrideException();
        }
        if (directoryPath != null) {
            throw new SimilarArgumentSetException();
        }
        directory = dir;
    }

    public Optional<Directory> getDirectoryArgument() throws ArgumentNotSetException {
        if (directory == null && directoryPath == null) {
            throw new ArgumentNotSetException();
        }
        return Optional.ofNullable(directory);
    }

    public void setDirectoryPathArgument(String dirPath) throws ArgumentOverrideException, SimilarArgumentSetException {
        if (directoryPath != null) {
            throw new ArgumentOverrideException();
        }
        if (directory != null) {
            throw new SimilarArgumentSetException();
        }
        directoryPath = dirPath;
    }

    public Optional<String> getDirectoryPathArgument() throws ArgumentNotSetException {
        if (directory == null && directoryPath == null) {
            throw new ArgumentNotSetException();
        }
        return Optional.ofNullable(directoryPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.CREATE;
    }
}


