package FS.actions;

import FS.Directory;


public class Tree extends FSAction<Void, Directory> {
    public Tree(Directory target) {
        super(target);
    }

    public Tree(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.TREE;
    }
}

