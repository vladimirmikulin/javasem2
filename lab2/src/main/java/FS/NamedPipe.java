package FS;

import FS.markers.Readable;
import java.util.concurrent.Semaphore;

public class NamedPipe extends Entity implements Readable<String> {
    private String data;
    private final Semaphore semaphoreReader;
    private final Semaphore semaphoreWriter;

    private NamedPipe(String name, Directory parent) {
        super(name, parent);
        data = "";
        semaphoreReader = new Semaphore(1);
        semaphoreWriter = new Semaphore(1);
        semaphoreReader.acquireUninterruptibly();
    }

    public static NamedPipe create(String name, Directory parent) {
        return new NamedPipe(name, parent);
    }

    public void write(String data) {
        if (data == null) {
            throw new IllegalArgumentException("Data should not be null");
        }
        semaphoreWriter.acquireUninterruptibly();
        this.data = data;
        semaphoreReader.release();
    }

    public String read() {
        semaphoreReader.acquireUninterruptibly();
        var res = data;
        semaphoreWriter.release();
        return res;
    }

    @Override
    public EntityType getType() {
        return EntityType.NAMED_PIPE;
    }

    @Override
    public int getSize() {
        return data.length();
    }
}


