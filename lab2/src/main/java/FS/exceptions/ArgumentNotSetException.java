package FS.exceptions;

public class ArgumentNotSetException extends Exception {
    public ArgumentNotSetException() {
        super("Couldn't get argument of action, it haven't been set yet");
    }
}
