package FS.exceptions;

public class SimilarArgumentSetException extends Exception {
    public SimilarArgumentSetException() {
        super("Similar argument has been set already");
    }
}
