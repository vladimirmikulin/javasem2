package FS.exceptions;

public class ArgumentOverrideException extends Exception {
    public ArgumentOverrideException() {
        super("Couldn't set argument, it couldn't be overridden");
    }
}

