package FS.markers;

public interface Readable<T> {
    T read();
}
